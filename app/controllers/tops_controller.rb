class TopsController < ApplicationController
  def main
  end
  def login
  end

  def create
    @user = User.find_by(uid: session_params[:uid], password: session_params[:password])

    if @user
       redirect_to tops_main_url, notice: 'ログインしました。'
    else
      render :login
    end
  end

  def destroy
    reset_session
    redirect_to root_url, notice: 'ログアウトしました。'
  end

  private
  def session_params
    params.require(:session).permit(:uid, :password)
  end

end
